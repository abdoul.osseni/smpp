FROM debian:bullseye-slim
RUN apt-get update && apt-get install -y kannel kannel-extras vim supervisor
COPY kannel.conf /etc/kannel/kannel.conf
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
CMD ["/usr/bin/supervisord"]
